import React, { Component } from "react";

import ChatIcon from "@material-ui/icons/Chat";
import { connect } from "react-redux"
import { setCat } from '../../redux/actions'

import {
  ListItem,
  ListItemIcon,
  ListItemText,
  Divider,
} from "@material-ui/core";

import "./BotContainer.css";

export class Bot extends Component {
  render() {
    return (
      <div>
        <ListItem
          button
          key={this.props.bot.id}
          // dispatch action setCurrentBot
          onClick={() => this.props.setCat(this.props.bot)}
        >
          <ListItemIcon>
            <img
              className={this.props.bot.class}
              alt={this.props.bot.class}
              src={this.props.bot.avatar}
            />
          </ListItemIcon>
          <ListItemText primary={this.props.bot.name} />
          <ChatIcon />
        </ListItem>
        <Divider />
      </div>
    );
  }
}

export default connect(null, { setCat })(Bot)