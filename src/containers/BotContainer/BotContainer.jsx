import React, { Component } from "react";
import { Grid, List, ListSubheader } from "@material-ui/core";
import Bot from "./Bot";
import { connect } from 'react-redux'

import "./BotContainer.css";

export class BotContainer extends Component {
  render() {
    return (
      <Grid
        item
        xs={2}
        style={{
          height: "calc(100vh - 75px)",
        }}
      >
        <List
          subheader={
            <ListSubheader component="div">Chats disponibles</ListSubheader>
          }
        >
          {this.props.bots.map((bot) => (
            <Bot
              bot={bot}
              key={bot.id}
            />
          ))}
        </List>
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  return { bots: state.chatReducer.bots }
}

export default connect(
  mapStateToProps
)(BotContainer);

