import { ADD_MESSAGE, SET_CAT } from './actionTypes';

export const addMessage = (currentBotId, message) => ({
    type: ADD_MESSAGE,
    payload: {
        currentBotId,
        message
    }
});

export const setCat = (bot) => ({
    type: SET_CAT,
    payload: {
        bot
    }
});