import { ADD_MESSAGE, SET_CAT } from "../actionTypes";
import { catBots } from "../../Bots/bots";

const initialState = {
    bots: catBots,
    currentBot: catBots[0]
}

export default function (state = initialState, action) {
    switch (action.type) {
        case ADD_MESSAGE: {
            const { currentBotId, message } = action.payload;
            console.log(state.bots[0])
            return {
                ...state,
                bots: state.bots.map((bot) =>
                    bot.id === currentBotId ? { ...bot, messages: [...bot.messages, message] } : bot
                )
            }
        }
        case SET_CAT: {
            return {
                ...state,
                currentBot: action.payload.bot
            }
        }
        default:
            return state
    }
}