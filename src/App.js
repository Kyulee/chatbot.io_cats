import React, { Component } from "react";

import Navbar from "./layout/Navbar";
import BotContainer from "./containers/BotContainer";
import ChatContainer from "./containers/ChatContainer";
import Homepage from "./layout/Homepage";

import { Box, Grid } from "@material-ui/core";
import { connect } from "react-redux"

import "./App.css";

export class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Box>
        <Navbar />
        <Grid
          container
          style={{ marginTop: "75px", height: "calc(100vh - 75px)" }}
        >
          <BotContainer
          />
          {this.props.currentBot ? (
            <ChatContainer />
          ) : (
            <Homepage />
          )}
        </Grid>
      </Box>
    );
  }
}

const mapStateToProps = state => {
  return { bots: state.chatReducer.bots, currentBot: state.chatReducer.currentBot }
}

export default connect(
  mapStateToProps
)(App);

